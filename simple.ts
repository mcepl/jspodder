/// <reference path='es6-promise.d.ts' />

export function getURL(inURL: string, method: string = "GET"):Promise<JSON> {
  var response: string,
    status: number,
    readyState: number,
    responseText: string;

  return new Promise<JSON>((resolve, reject) => {
    var xhr = new XMLHttpRequest();

    xhr.open(method, inURL, true);

    xhr.onload = event => {
      if (this.status === 200) {
        if (this.responseType === 'json') {
          resolve(this.response);
        }
        else {
          reject("Expected type of response to be 'json', got " +
            this.responseType);
        }
      }
    };

    xhr.onerror = event => reject(xhr.statusText);

    xhr.send(null);

  });
}
