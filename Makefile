REPORTER ?= spec
FILES=simple.js test/simple.js

%.js: %.ts
	tsc --sourcemap -t ES5 -m commonjs $<

all: $(FILES)

install: $(FILES)
	rsync -avLz --delete $(FILES) /home/matej/Dokumenty/website/luther/hesla/
	websync

test: $(FILES)
	mocha --reporter $(REPORTER) --ui tdd --growl test/*.js

clean:
	find . \( -name \*.js -o -name \*.js.map \) -print -delete

.PHONY: test clean
